/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benjamas.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author sany
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }

    public static ArrayList<Product> genProductList() {
         ArrayList<Product> list = new ArrayList<>();
         list.add(new Product(1,"Hot Coffee",30,"hot coffee.png"));
         list.add(new Product(2,"Pink Milk",40,"pink milk.jpg"));
         list.add(new Product(3,"Cocoa",50,"cocoa.jpg"));
         list.add(new Product(4,"Hot Green Tea",30,"hot green tea.jpg"));
         list.add(new Product(5,"Hot Milk",40,"hot milk.png"));
         list.add(new Product(6,"Hot Thai Tea",50,"hot thai tea.png"));
         list.add(new Product(7,"Iced Mocha",30,"iced mocha.png"));
         list.add(new Product(8,"Iced Milk",40,"iced milk.png"));
         list.add(new Product(9,"Thai Tea",50,"thai tea.png"));

         
         return list;
    }
}
